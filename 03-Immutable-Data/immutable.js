const meal = {
	id: 1,
	description: 'Breakfast',
};

const updatedMeal = {
	...meal,
	description: 'Brunch',
	calories: 600,
};

// console.log(meal, updatedMeal);

// Destructuring
// const { description, calories } = updatedMeal;
// console.log(description, calories);

// Rest to delete properties from an object
const { id, ...mealWithoutId } = updatedMeal;

// console.log(mealWithoutId);

/** Exercise:
 * In an immutable way, add a property to the meal called calories setting
 * its value to 200, then log the result to the console.
 *
 * In an immutable way, increase the calories by 100 and print the result
 * to the console.
 *
 * In an immutable way, remove the calories property and log the result
 */

const meal2 = {
	description: 'Dinner',
};

const updatedMeal2 = {
	...meal2,
	calories: 200,
};

// console.log(updatedMeal2);

const updatedMeal3 = {
	...updatedMeal2,
	calories: 300,
};

// console.log(updatedMeal3);

const { calories, ...mealWithoutCalories } = updatedMeal3;

// console.log(mealWithoutCalories);

/** Array Immutability exercise
 * 1. Create a constant named friends, which is an array containing two names */
const friends = ['Jair', 'Fer'];

/** 2. Create new constant named updatedFriends, which includes the friends array
 * values plus one additional name. */
const updatedFriends = [...friends, 'Karla'];

/** 3. Create a new constant named friendNameLengths, which is based on the array
 * updatedFriends but instead of having the friends names, have the array store
 * the length of each persons name */
const friendNameLengths = updatedFriends.map((name) => name.length);

/** 4. Create a new constant named shorterNamedFriends, which will be a list of the
 * friends except the friend with the longest name. */
const shorterNameFriends = updatedFriends.filter((name) => name.length < Math.max(...friendNameLengths));

// Console all variables
// console.log({
// 	friends,
// 	updatedFriends,
// 	friendNameLengths,
// 	shorterNameFriends,
// });

const reviews = [4.5, 4.0, 5.0, 2.0, 1.0, 5.0, 3.0, 4.0, 1.0, 5.0, 4.5, 3.0, 2.5, 2.0];

/**
 * 1. Using the reduce function, create an object that
 * has properties for each review value, where the value
 * of the property is the number of reviews with that score.
 * for example, the answer should be shaped like this:
 * { 4.5: 1, 4: 2 ...}
 * Tip: Use computed property names: According to documentation, we can use []
 * to compute a property name, which will resolve an expression to a property name,
 */

const reviewCounter = reviews.reduce((acc, review) => {
	return {
		...acc,
		[review]: acc[review] + 1 || 1,
	};
}, {});

console.log({ reviewCounter });
