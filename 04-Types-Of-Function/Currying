========================= CURRYING =====================================

'A function that will return a new function until it receives all its arguments'

Take for example the next function

const greet = (greeting, name) => `${greeting} ${name}`;

And then we console log a call to that function
console.log('Good morning', 'Darío');

Pretty straight forward huh?, But what if i have a list of names and want to
call all the elements in the list through my greet function.
What function will help us pipe all the elements through a function? That's right,
the map. This Array.map function is essential to fp and is in the 80/20 rule of js fp.

but how do we pass two parameters to a map function?
Lets take for instance the list of names we have:
const friends = ['Nate', 'Jim', 'Scott', 'Dean'];

and lets map that array to the greet function, it receives two parameters right?

const friendGreetings = friends.map(greet);

Then we get
['Nate 0', 'Jim 1',...]

The map fn is passing the name as the first parameter in the greet function, and the array
index as the second one. So, how do we solve this problem?
Lets remember that functions are values just as objects or arrays or strings, and can be passed
around as parameters and can be returned by other functions.

With these premises let's refactor the greet function. Also remeber that the map function only 
passes the array element as the first parameter of its eval function.

First with normal functions, then with arrows.

function greet(greeting) {
  return function(name) {
    return `${greeting} ${name}`;
  }
}

Then how do we use this like in the first single simple example? lets remember the greet
fn returns another fn. So we can do something like this

const fn = greet('Good Morning')

Then call it again:

console.log(fn('Darío)) // 'Good Morning Darío'

Good right? Not lets condense it in one call
console.log(greet('GoodMorning')('Darío'));

now we could pipe this through a map fn. But, if we pass it like this, we will have
an array of functions. Each will be the inner fn. So we need to call the function in the
map function with a parameter

const friendGreetings = friends.map(greet('Good Morning'));

This array will contain all the correct greetings then. What happened here?
We need to learn the concept of High Order Functions.

======================= HIGH-ORDER FUNCTION ===========================
A high order function is a fn that either takes a fn as an argument as a parameter, and/or
returns a function.
In the previous example we saw several application of the HOF, map is a hof because it takes
a fn as a parameter. Greet is a hof because it returns a fn.

There are a couple of interesting things on the greet fn worth noting, let's look at it again.

function greet(greeting) {
  return function(name) {
    return `${greeting} ${name}`;
  }
}

See how the inner function has access to the greeting param as well? 
This is called a CLOSURE
Closures are functions that can access to variables that aren't explicitly passed into the function
because of the placement of the functions relative to the variables.

In the example: The param greeting can be used in any place between its function code block. That is
what is known as 'the scope of the parameter', since the return fn is within the scope, it has
access to the greeting param. This is closure by definition.

This transformation of a function that takes two arguments and returns a string, to a function
that takes one parameter and returns a function which receives the remaining argument, is called
currying.

Currying is quite helpful, the first version of the greet function, didn't work well with map,
but the curried version did.

There's a concept related to currying that is called Partial application.

