======================== Partial Application ========================

In the last example of the greet curried function:

//Regular version
function greet(greeting) {
  return function(name) {
    return `${greeting} ${name}`;
  }
}

// Lambda version
const greet = greeting => name => `${greeting} ${name}`;

Partial application is what we did when we set a greeting In

const morningGreet = greet('Good morning');

We can think of partial application as 'specializing a general function', and we could've
created a specialized greeting version of evening and afternoon, or night as well.

The key to understand the difference between currying an partial application is
that currying is what you to to an original function before you actually use that function.
eg, the transformation of the greet function that returned the string into a fn that returns a fn.

And partial application is actually using the curried function in part with some of the parameters
that the function need, but not all of them.
eg: the specialized morning greet.

Can we use partial application without a curried function? or we necessarily need a curried function?
We can use partial application it without a curried function, but we need a helper function.

For example:
Lets imagine we have a simple function from a library.

function add(x, y) {
  return x + y;
}

Then for some reason we need to apply partial application to this fn but we don't want to make
a curried version of it. eg, lets say we want to stablish a 3 to add to a second parameter
so we create a constant that will hold the partially applicated function with the set value
of 3, which is a result of the partial helper. Lets see:

const add3 = partial(add, [3]);

we call the partial with the fn (add) where we want to apply the partial Application, and the 
first parameter to specialize that function.

Here's the key difference between currying and partial application:
Curry: Is about CREATING FUNCTIONS WITH NO DATA
Partial Application: Consuming or using functions WITH DATA

Partial Application can be applied to curried and regular fns (with helper functions).

As a FN programmers we will be using currying and partial application ALL THE TIME.
These are one of those 20% Items that will be used in 80% of the code we write.

When using currying, does the order of the parameters matter?
Yes they do, the more inner function should be the one that is more specialized, the one
that the nested function is acting on.

Lets take the greeting example. the general greeting is acting in all the names on the list,
this is why they needed to be the second parameter.

['James', 'Darío'].map(morningGreet); // ['Good morning James', 'Good morning Darío']

Lets be aware that javascript does not care about currying at all, so we end with a verbose way
of calling the function with one result.
eg:

greet('Good morning')('James');

To solve this issue there's this library focused on functional programming called Ramda
which does the currying for us.

lets see with the initial non curried version of the fn:

const greet = (greeting, name) => `${greeting} ${name}`;

then we apply a ramda fn called curry.

const greet = R.curry((greeting, name) => `${greeting} ${name}`);

This returns a curried version of the original function and also allow us to call the fn with
the same syntax as always.

greet('Good Morning', 'James'); 

The good thing about ramda is that all of its functions are curried by default and they never
mutate any data.
