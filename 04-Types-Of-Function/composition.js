const R = require('ramda');

const sentence = `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium tenetur tempora voluptate sed aspernatur deleniti.`;

const wordList = R.split(' ', sentence);
console.log(wordList);

// We could replace the wordList with a call to R.split
const wordCount = R.length(wordList);
console.log(wordCount);
