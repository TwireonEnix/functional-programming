const greet = (greeting, name) => `${greeting} ${name}`;
console.log(greet('Good Morning', 'Darío'));

const friends = ['Nate', 'Jim', 'Scott', 'Dean'];
const greet2 = (greeting) => (name) => `${greeting} ${name}`;

const friendsGreetings = friends.map(greet2('Good Morning'));
console.log(friendsGreetings);
