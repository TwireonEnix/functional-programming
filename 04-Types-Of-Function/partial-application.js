const greet = (greeting) => (name) => `${greeting} ${name}`;

function greet2(greeting) {
	return function(name) {
		return `${greeting} ${name}`;
	};
}

// console.log(greet('Hello')('Darío') === greet2('Hello')('Darío'));
const morningGreet = greet('Good morning');

const greetings = ['James', 'Darío'].map(morningGreet); // ['Good morning James', 'Good morning Darío']
console.log(greetings);
