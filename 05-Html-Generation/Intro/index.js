// object tags was imported with the hyperscript import in the html, this object contains functions
// that generate all available html tags
// const myHeading = tags.h1('Hello World');
const { h1, p } = tags;
const myHeading = h1('Hello World');
console.log(myHeading.outerHTML); // <h1>Hello Workd</h1>

// Here we interact with the dom. And mount the element into the node.
// However this is impure code. Console.logs are also impure because they cause side effects.
// If we cannot get rid of the impure code we must control it as much as possible, for example:
// Rendering data in the dom is a side effect from a function, therefore it is impure, here we
// must use impure functions
const node = document.getElementById('hello');
node.appendChild(myHeading);

// EXERCISE
// 1. using destructuring, create a constant named
// p, which is a function that will create paragraph tags
// Remember, you'll need to use the "tags" namespace
// to destructure from

// 2. create a constant named myParagraph by using the
// p function you coded in step 1.  The paragraph should
// contain a sentence of your choosing.
const sentence = p(`Hello, I'm trying to learn functional programming.`);

// 3. create a constant named node, that references
// the dom node where the id is 'app'.
const nodeP = document.getElementById('exercise');

// 4. add your 'myParagraph' to the dom node you
// coded in step 3
nodeP.appendChild(sentence);

// HINTS / HELP
// If you need help, you can read the following docs:
// hyperscript-helpers: https://github.com/ohanhi/hyperscript-helpers
// destructuring: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment#Object_destructuring
// reference dom node: https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById
// adding html in code: https://developer.mozilla.org/en-US/docs/Web/API/Node/appendChild
// you can find my solution at: https://jsbin.com/najisaw/1/edit?js,output

// APPEND VS APPEND CHILD
// ParentNode.append() allows you to also append DOMString object, whereas Node.appendChild() only accepts Node objects.
// ParentNode.append() has no return value, whereas Node.appendChild() returns the appended Node object.
// ParentNode.append() can append several nodes and strings, whereas Node.appendChild() can only append one node.
