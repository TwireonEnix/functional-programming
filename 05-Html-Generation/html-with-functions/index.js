// Data:
const meals = [
	{ description: 'Breakfast', calories: 460 },
	{ description: 'Snack', calories: 180 },
	{ description: 'Lunch', calories: 600 },
];

/**
 * First order function that receives another function as a parameter (tag in this case),
 * Tag functions receive and object as the first parameter that will have the html properties
 * like class, id, etc, the tags destructuring returns functions, not objects.
 */
const { td, th, tr, tbody, thead, table } = tags;

const cell = (tag, className, value) => tag({ className }, value);

const mealRow = className => meal =>
	tr({ className }, [cell(td, 'pa2', meal.description), cell(td, 'pa2 tr', meal.calories)]);

/**
 * In the mealsBody function, James use heavily the Ramda library to use the map and partial
 * application functions, but i preferred to curry my meal function and apply partial aplication
 * myself, here's James row code, somewhat cleaner but more confusing (to me at least at this point):
 * const rows = R.map(R.partial(mealRow), ['stripe-dark']), meals);
 */
const mealsBody = (className, meals) => {
	const stripedMealRow = mealRow('stripe-dark');
	const rows = meals.map(meal => stripedMealRow(meal));
	const rowsWithTotal = R.append(totalRow(meals), rows);
	return tbody({ className }, rowsWithTotal);
};

// Constant because the header never changes.
const headerRow = tr([cell(th, 'pa2 tl', 'Meal'), cell(th, 'pa2 tr', 'Calories')]);
const mealHeader = thead(headerRow);

// Here james uses ramda again, but i hesitate to use a library if there are native js functions I
// can work with, I guess somewhere i'll need ramda but i still don't.
/**
 * James code: Pipe is a function composition which works from top (or left) to bottom (or right);
 * const total R.pipe(R.map(meal => meal.calories), R.reduce((acc, calories) => acc + calories, 0))(meals);
 */
const totalRow = meals => {
	const total = meals.map(meal => meal.calories).reduce((acc, calories) => acc + calories, 0);
	return tr({ className: 'bt b' }, [cell(td, 'pa2 tr', 'Total:'), cell(td, 'pa2 tr', total)]);
};

const mealsTable = meals => table({ className: 'mw5 center w-100 collapse' }, [mealHeader, mealsBody('', meals)]);

const node = document.getElementById('app');
const view = mealsTable(meals);
node.appendChild(view);
