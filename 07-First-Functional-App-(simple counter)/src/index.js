const { div, button } = tags;

const initModel = 0;

/** We will start with the view functions, let's remember the core rules of pure functions
 * 1. Have input parameters
 * 2. No stateful values
 * 3. Return based on input
 * 4. NO side effects
 */
const view = (dispatch, model) => {
	// maybe render functions convert html templates to this?
	return div([
		div({ className: 'mv2' }, `Count: ${model}`),
		button({ className: 'pv1 ph2 mr2', onclick: () => dispatch(messages.add) }, '+'),
		button({ className: 'pv1 ph2', onclick: () => dispatch(messages.subtract) }, '-'),
	]);
};

const messages = {
	add: 'add',
	subtract: 'subtract',
};

const update = (msg, model) => {
	switch (msg) {
		case messages.add:
			return model + 1;
		case messages.subtract:
			return model - 1;
		default:
			return model;
	}
};

// Impure code here (side effects to dom and state.)
const app = (initModel, update, view, node) => {
	let model = initModel; // this holds the state of the app
	let currentView = view(dispatch, model);
	node.appendChild(currentView);
	function dispatch(msg) {
		model = update(msg, model);
		const updatedView = view(dispatch, model);
		node.replaceChild(updatedView, currentView); // replace Child receives two parameters, new replaces old
		currentView = updatedView;
	}
};

const rootNode = document.getElementById('app');

app(initModel, update, view, rootNode);
