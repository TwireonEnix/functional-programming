import { diff, patch } from 'virtual-dom';
import createElement from 'virtual-dom/create-element';

const app = (initModel, update, view, node) => {
	let model = initModel; // this holds the state of the app
	let currentView = view(dispatch, model);
	// next line uses the virtual dom library
	let rootNode = createElement(currentView);
	node.appendChild(rootNode);
	function dispatch(msg) {
		model = update(msg, model);
		const updatedView = view(dispatch, model);
		// Using the virtual dom functions to search the differences in the actual dom
		// and just replaces that section with the patch function.
		// node.replaceChild() no longer needed.
		const patches = diff(currentView, updatedView);
		rootNode = patch(rootNode, patches);
		currentView = updatedView;
	}
};

export default app;
