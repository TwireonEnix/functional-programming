export default {
	description: 'Breakfast',
	calories: 100,
	showForm: false,
	nextId: 0,
	editId: null,
	meals: [],
};
