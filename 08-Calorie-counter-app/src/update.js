import * as R from 'ramda';

const messages = {
	show_form: 'show_form',
	meal_input: 'meal_input',
	calories_input: 'calories_input',
	save_meal: 'save_meal',
	delete_meal: 'delete_meal',
	edit_meal: 'edit_meal',
};

export const showFormMsg = showForm => ({
	type: messages.show_form,
	showForm,
});

export const mealInputMessge = description => ({
	type: messages.meal_input,
	description,
});

export const caloriesInputMessage = calories => ({
	type: messages.calories_input,
	calories,
});

export const saveMealMsg = { type: messages.save_meal };

const add = (msg, model) => {
	const { nextId, description, calories } = model;
	const meal = { id: nextId, description, calories };
	const meals = [...model.meals, meal];
	return {
		...model,
		meals,
		nextId: nextId + 1,
		description: '',
		calories: 0,
		showForm: false,
	};
};

export const deleteMealMsg = id => ({
	type: messages.delete_meal,
	id,
});

export const editMealMsg = editId => ({
	type: messages.edit_meal,
	editId,
});

export const edit = (msg, model) => {
	const { description, calories, editId } = model;
	const meals = R.map(meal => (meal.id === editId ? { ...meal, description, calories } : meal), model.meals);
	return {
		...model,
		meals,
		description: '',
		calories: 0,
		showForm: false,
		editId: null,
	};
};

const update = (msg, model) => {
	switch (msg.type) {
		case messages.show_form: {
			const { showForm } = msg;
			// override showForm in model
			return { ...model, showForm, description: '', calories: 0 };
		}

		case messages.meal_input: {
			const { description } = msg;
			return { ...model, description };
		}

		case messages.calories_input: {
			// const { calories } = msg;
			/**
			 * convert the calories to a number using functional composition using Ramda library,
			 * R.pipe is like R.compose but the piping is from top to bottom (or left to right),
			 * R.defaultTo is a function that returns the parameter if the input is NaN, null or
			 * undefined.
			 */
			const calories = R.pipe(
				parseInt,
				R.defaultTo(0),
			)(msg.calories);
			return { ...model, calories };
		}

		case messages.save_meal: {
			// Changing the save logic to see if it's a new meal or an existing one
			const { editId } = model;
			/**
			 * BUG here, if we just check for truthy values, as 0 is a falsy one,
			 * the first index of the array will cause a duplicate of it.
			 * so let's change editId ?... to editId !== null ?...
			 */
			return editId !== null ? edit(msg, model) : add(msg, model);
		}

		case messages.delete_meal: {
			const { id } = msg;
			const meals = R.filter(meal => meal.id !== id, model.meals);
			return { ...model, meals };
		}
		case messages.edit_meal: {
			const { editId } = msg;
			const meal = R.find(meal => meal.id === editId, model.meals);
			const { description, calories } = meal;
			return {
				...model,
				editId,
				description,
				calories,
				showForm: true,
			};
		}
	}
	return model;
};

export default update;
