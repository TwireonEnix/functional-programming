import * as R from 'ramda';

export const messages = {
	left_value_input: 'left_value_input',
	right_value_input: 'right_value_input',
	left_dropdown_change: 'left_dropdown_change',
	right_dropdown_change: 'right_dropdown_change',
};

export const leftValueInputMsg = leftValue => ({
	type: messages.left_value_input,
	leftValue,
});

export const rightValueInputMsg = rightValue => ({
	type: messages.right_value_input,
	rightValue,
});

export const leftDropdownMsg = leftUnit => ({
	type: messages.left_dropdown_change,
	leftUnit,
});

export const rightDropdownMsg = rightUnit => ({
	type: messages.right_dropdown_change,
	rightUnit,
});

const toFloat = value =>
	R.pipe(
		parseFloat,
		R.defaultTo(0),
	)(value);

const FtoC = value => (5 / 9) * (value - 32);
const CtoF = value => (9 / 5) * value + 32;
const KtoC = value => value - 273.15;
const CtoK = value => value + 273.15;
const FtoK = R.pipe(
	FtoC,
	CtoK,
);
const KtoF = R.pipe(
	KtoC,
	CtoF,
);

const unitConversions = {
	Celsius: {
		Fahrenheit: CtoF,
		Kelvin: CtoK,
	},
	Fahrenheit: {
		Celsius: FtoC,
		Kelvin: FtoK,
	},
	Kelvin: {
		Celsius: KtoC,
		Fahrenheit: KtoF,
	},
};

const convertFromTemp = (fromUnit, toUnit, temp) => {
	/**
	 * This line of logic is somewhat convoluted. What this does is that through
	 * R.identity, there is no function returned from above because if both sides
	 * have the same unit, it does not need to perform any conversion.
	 * The pathOr allows us to search for a value in a nested object, in
	 * this case unitConversions, to do something like this:
	 * unitConversions.fromUnit.toUnit
	 * This will bring the function reference that we need to make the conversion.
	 * Ramda introduces some abstraction that regular programmers are not used to,
	 * like me before this.
	 */
	const convertFn = R.pathOr(R.identity, [fromUnit, toUnit], unitConversions);
	return convertFn(temp);
};

const round = value => Math.round(value * 100) / 100;

const convert = model => {
	const { leftValue, leftUnit, rightValue, rightUnit } = model;
	const [fromUnit, fromTemp, toUnit] = model.sourceLeft
		? [leftUnit, leftValue, rightUnit]
		: [rightUnit, rightValue, leftUnit];
	const otherValue = R.pipe(
		convertFromTemp,
		round,
	)(fromUnit, toUnit, fromTemp);
	return model.sourceLeft ? { ...model, rightValue: otherValue } : { ...model, leftValue: otherValue };
};

function update(msg, model) {
	switch (msg.type) {
		case messages.left_value_input: {
			if (msg.leftValue === '') return { ...model, sourceLeft: true, leftValue: '', rightValue: '' };
			const leftValue = toFloat(msg.leftValue);
			return convert({ ...model, sourceLeft: true, leftValue });
		}
		case messages.right_value_input: {
			if (msg.rightValue === '') return { ...model, sourceLeft: false, leftValue: '', rightValue: '' };
			const rightValue = toFloat(msg.rightValue);
			return convert({ ...model, sourceLeft: false, rightValue });
		}
		case messages.left_dropdown_change: {
			const { leftUnit } = msg;
			return convert({ ...model, leftUnit });
		}
		case messages.right_dropdown_change: {
			const { rightUnit } = msg;
			return convert({ ...model, rightUnit });
		}
	}
	return model;
}

export default update;
