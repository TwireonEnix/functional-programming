import * as R from 'ramda';
import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';
import { leftValueInputMsg, rightValueInputMsg, leftDropdownMsg, rightDropdownMsg } from './update';

const { div, h1, pre, select, option, input } = hh(h);

const units = ['Fahrenheit', 'Celsius', 'Kelvin'];

const unitOptions = selectedUnit =>
	R.map(unit => option({ value: unit, selected: selectedUnit === unit }, unit), units);

const unitSection = (dispatch, unit, value, inputMsg, dropdownMsg) =>
	div({ className: 'w-50 ma1' }, [
		input({
			type: 'number',
			className: 'db w-100 mv2 pa2 input-reset ba',
			value,
			oninput: e => dispatch(inputMsg(e.target.value)),
		}),
		select(
			{
				className: 'db w-100 pa2 ba input-reset br1 bg-white ba b--black',
				onchange: e => dispatch(dropdownMsg(e.target.value)),
			},
			unitOptions(unit),
		),
	]);

function view(dispatch, model) {
	return div({ className: 'mw6 center' }, [
		h1({ className: 'f2 pv2 bb' }, 'Temperature Unit Converter'),
		div({ className: 'flex' }, [
			unitSection(dispatch, model.leftUnit, model.leftValue, leftValueInputMsg, leftDropdownMsg),
			unitSection(dispatch, model.rightUnit, model.rightValue, rightValueInputMsg, rightDropdownMsg),
		]),
		pre(JSON.stringify(model, null, 2)),
	]);
}

export default view;
