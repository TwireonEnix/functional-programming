const R = require('ramda');
const greeting = (greeting, name) => `${greeting} ${name}`;

// console.log(greeting('Good morning', 'James'));

const friends = ['Nate', 'Scott', 'Dean'];

const friendGreetings = friends.map(greeting);
// console.log(friendGreetings);

const curriedGreeting = greeting => name => `${greeting} ${name}`;

const morningGreeting = curriedGreeting('Good Morning');

const friendGoodGreeting = friends.map(morningGreeting);
// console.log(friendGoodGreeting);

/** High order functions: Takes a fn as a parameter, and or returns a function
 * Closure: Functions that can access and use variables that aren't explicitly passed into the function
 * but can use them because of their declaration relative to the variables, example: the curried
 * greeting function.
 *
 * The parameter greeting can be used anywhere between the scope of the function, since the return function is
 * within the scope of the high order function, the return function has access to the greeting parameter.
 *
 * Currying: What you do to a function before using that function, the curried was applied to the original greeting
 * function to enable it to accept just one parameter and return a function with the remaining one.
 *
 * Currying is about creating functions, without any data
 *
 * Partial Application: Specializing a general function, and use an already currying the function. An example is what
 * i did when I created the morning greeting function.
 *
 * Partial application is when we actually use a function (after currying it, by hand or with a helper function that curries it)
 * with actual data.
 *
 * As a fnp we will be using currying and partial application almost all the time, it is inside of the 20% js to be used 80%
 *
 * It is important to note that parameter order is important in the currying and partial application concepts. The most general parameter
 * goes first, then the more specialized functions in that order.
 *
 *
 * Apps are just data and transformations of that data.
 * FNP use functions to transform that data
 *
 * There are two kind of functions:
 *
 * Pure functions and procedures.
 * Pure function: creates and returns values based on its input and have no observable side effects
 * - 1 Pure function must have input parameters
 * - 2 No stateful values, the body must not depend of any parameter or value outside of itself
 * - 3 It must return values based in its input
 * - 4 It must not cause side effects
 *
 * Side effect?: When code changes something outside of its scope. Examples:
 * - Writing to a db or file
 * - Changing the DOM in a web app
 *
 * To a function to be considered pure it must not break any of the 4 stated rules, even if there's only 1 broken it is not a pure function.
 *
 * Writing pure function is not easy but they are reusable, composable, easy to test.
 *
 * Function composition: Making functions out of other function by combining their inputs and outputs
 * Example
 *
 * function slice(apple) {
 *  return slicedApple
 * }
 *
 * function bake(slicedFruit) {
 *  return pie;
 * }
 *
 * Composing:
 *
 * function makePie = compose(bake, slice);
 * const applePie = makePie(apple);
 */

const sentence =
	'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquid architecto reprehenderit eum, perspiciatis quaerat voluptate illum odio suscipit repellendus nesciunt maxime earum inventore, ipsa unde laboriosam? Fugit similique possimus quis? Eos, numquam natus iusto a voluptatibus ut dolorum at aut?';

const wordLength = sentence.split(' ').length;

const countWords = R.compose(
	R.length,
	R.split,
);
const countPipe = R.pipe(
	R.split(' '), // using partial application as all of ramda's functions are curried by default
	R.length,
);
const words = countWords(' ', sentence);
const wordsP = countPipe(sentence);

console.log({ words, wordsP });
