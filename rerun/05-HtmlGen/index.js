const { td, th, tr, tbody, thead, table } = tags;
const MEALS = [
	{ description: 'Lunch', calories: 600 },
	{ description: 'Breakfast', calories: 470 },
	{ description: 'Snack', calories: 180 },
];

// single responsibility principle
// each function must have only one task
/**
 * Plan / Functions to create
 * - cell
 * - mealRow
 * - headerRow
 * - totalRow
 * - mealBody
 * - mealHeader
 * - mealsTable
 */

const cell = (tag, className, value) => tag({ className }, value);

const mealRow = (className, meal) =>
	tr({ className }, [cell(td, 'pa2', meal.description), cell(td, 'pa2 tr', meal.calories)]);

const mealBody = (className, meals) => {
	// R.partial allows us to perform partial application to a function that is not curried,
	// The first param is the function we want to apply the partial application and the second is an array with the
	//parameters we want to partially apply to the function. This returns a partial
	const rows = R.map(R.partial(mealRow, ['stripe-dark']), meals);
	const rowsWithTotal = R.append(totalRow(meals), rows);
	return tbody({ className }, rowsWithTotal);
};

const headerRow = tr([cell(th, 'pa2 tl', 'Meal'), cell(th, 'pa tr', 'Calories')]);

const mealHeader = thead(headerRow);

const mealsTable = meals => table({ className: 'mw5 center w-100 collapse' }, [mealHeader, mealBody('', meals)]);

const totalRow = meals => {
	// const total = R.pipe(
	// 	R.map(meal => meal.calories),
	// 	R.reduce((acc, calories) => acc + calories, 0),
	// )(meals);
	const total = meals.map(meal => meal.calories).reduce((a, c) => a + c, 0);
	return tr({ className: 'bt b' }, [cell(td, 'pa2 tr', 'Total:'), cell(td, 'pa2 tl', total)]);
};

const node = document.getElementById('app');
const view = mealsTable(MEALS);
node.appendChild(view);
