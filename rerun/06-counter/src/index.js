import hh from 'hyperscript-helpers';
import { h, diff, patch } from 'virtual-dom;';
import { create } from 'virtual-dom';

const { div, button } = hh(h);

const initModel = 0;

const view = (model, dispatch) =>
	div([
		div({ className: 'mv2' }, `Count: ${model}`),
		button({ className: 'pv1 ph2 mr2', onclick: () => dispatch(1) }, '+'),
		button({ className: 'pv1 ph2', onclick: () => dispatch(0) }, '-'),
	]);

const update = (msg, model) => (msg ? model + 1 : model - 1);

// Impure code
const app = (initModel, update, view, node) => {
	let model = initModel;
	let currentView = view(model, dispatch);
	let rootNode = createElement(currentView);
	node.appendChild(rootNode);
	function dispatch(msg) {
		model = update(msg, model);
		const updatedView = view(model, dispatch);
		const patches = diff(currentView, updatedView);
		rootNode = patch(rootNode, patches);
		currentView = updatedView;
	}
};

const rootNode = document.getElementById('app');
app(initModel, update, view, rootNode);
