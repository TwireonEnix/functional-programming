import * as R from 'ramda';

const MSGS = {
	showForm: 'showForm',
	mealInput: 'mealInput',
	caloriesInput: 'caloriesInput',
	saveMeal: 'saveMeal',
	deleteMeal: 'deleteMeal',
	editMeal: 'editMeal',
};

export const saveMealMsg = { type: MSGS.saveMeal };

export const deleteMealMsg = id => ({
	type: MSGS.deleteMeal,
	id,
});

export const showFormMsg = showForm => ({
	type: MSGS.showForm,
	showForm,
});

export const mealInputMsg = description => ({
	type: MSGS.mealInput,
	description,
});

export const caloriesInputMsg = calories => ({
	type: MSGS.caloriesInput,
	calories,
});

export const editMealMsg = editId => ({
	type: MSGS.editMeal,
	editId,
});

const add = (msg, model) => {
	const { nextId, description, calories, meals } = model;
	const meal = { id: nextId, description, calories };
	const newMeals = [...meals, meal];
	return {
		...model,
		nextId: nextId + 1,
		description: '',
		calories: 0,
		meals: newMeals,
		showForm: false,
	};
};

const edit = (msg, model) => {
	const { editId, description, calories, meals } = model;
	const editMeals = R.map(
		meal =>
			meal.id === editId
				? {
						...meal,
						description,
						calories,
				  }
				: meal,
		meals,
	);
	return {
		...model,
		meals: editMeals,
		showForm: false,
		description: '',
		calories: 0,
		editId: null,
	};
};

const update = (msg, model) => {
	switch (msg.type) {
		case MSGS.showForm: {
			const { showForm } = msg;
			return { ...model, showForm, description: '', calories: 0 };
		}
		case MSGS.mealInput: {
			const { description } = msg;
			return { ...model, description };
		}
		case MSGS.caloriesInput: {
			const calories = R.pipe(
				parseInt,
				R.defaultTo(0),
			)(msg.calories);
			return { ...model, calories };
		}
		case MSGS.saveMeal: {
			const { editId } = model;
			// here null is needed because it cause a bug if the meal id is zero
			return editId !== null ? edit(msg, model) : add(msg, model);
		}
		case MSGS.deleteMeal: {
			const { id } = msg;
			const meals = R.filter(meal => meal.id !== id, model.meals);
			return { ...model, meals };
		}
		case MSGS.editMeal: {
			const { editId } = msg;
			const meal = R.find(meal => meal.id === editId, model.meals);
			const { description, calories } = meal;
			return {
				...model,
				editId,
				description,
				calories,
				showForm: true,
			};
		}
	}
	return model;
};

export default update;
