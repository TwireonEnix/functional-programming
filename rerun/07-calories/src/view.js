import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';
import { showFormMsg, mealInputMsg, caloriesInputMsg, saveMealMsg, deleteMealMsg, editMealMsg } from './update';
import * as R from 'ramda';

const { pre, div, h1, button, form, label, input, table, thead, tbody, tr, th, td, i } = hh(h);

/** Table generation */
const cell = (tag, className, value) => tag({ className }, value);

const tableHeader = thead([tr([cell(th, 'pa2 tl', 'Meal'), cell(th, 'pa2 tr', 'Calories'), cell(th, '', '')])]);

const mealRow = (dispatch, className, meal) =>
	tr({ className }, [
		cell(td, 'pa2', meal.description),
		cell(td, 'pa2 tr', meal.calories),
		cell(td, 'pa2 tr', [
			i({
				className: 'ph1 fa fa-trash-o pointer',
				onclick: () => dispatch(deleteMealMsg(meal.id)),
			}),
			i({
				className: 'ph1 fa fa-pencil-square-o pointer',
				onclick: () => dispatch(editMealMsg(meal.id)),
			}),
		]),
	]);

const totalRow = meals => {
	const total = R.pipe(
		R.map(meal => meal.calories),
		R.sum,
	)(meals);
	return tr({ className: 'bt b' }, [cell(td, 'pa2 tr', 'Total:'), cell(td, 'pa2 tr', total), cell(td, '', '')]);
};

const mealsBody = (dispatch, className, meals) => {
	const rows = R.map(R.partial(mealRow, [dispatch, 'stripe-dark']), meals);
	const rowsWithTotal = [...rows, totalRow(meals)];
	return tbody({ className }, rowsWithTotal);
};

const tableView = (dispatch, meals) =>
	meals.length === 0
		? div(
				{
					className: 'mv2 i black-50',
				},
				'No meals to display...',
		  )
		: table({ className: 'mv2 w-100 collapse' }, [tableHeader, mealsBody(dispatch, '', meals)]);

// Function that creates the text inputs
const fieldSet = (labelText, inputValue, oninput) =>
	div([
		label({ className: 'db mb1' }, labelText),
		input({ className: 'pa2 input-reset ba w-100 mb2', type: 'text', value: inputValue, oninput }),
	]);

// Function that creates the save / cancel buttons
const buttonSet = dispatch =>
	div([
		button(
			{
				className: 'f3 pv2 ph3 bg-blue white bn mr2 dim',
				type: 'submit',
			},
			'Save',
		),
		button(
			{
				className: 'f3 pv2 ph3 bn bg-light-grey dim',
				type: 'button',
				onclick: () => dispatch(showFormMsg(false)),
			},
			'Cancel',
		),
	]);

const formView = (dispatch, model) => {
	const { description, calories, showForm } = model;
	return showForm
		? form(
				{
					className: 'w-100 mv2',
					onsubmit: e => {
						e.preventDefault();
						dispatch(saveMealMsg);
					},
				},
				[
					fieldSet('Meal', description, e => dispatch(mealInputMsg(e.target.value))),
					fieldSet('Calories', calories || '', e => dispatch(caloriesInputMsg(e.target.value))),
					buttonSet(dispatch),
				],
		  )
		: button({ className: 'f3 pv2 ph3 bg-blue white bn', onclick: () => dispatch(showFormMsg(true)) }, 'Add meal');
};

const view = (dispatch, model) =>
	div({ className: 'mw6 center' }, [
		h1({ className: 'f2 pv2 bb' }, 'Calorie Counter'),
		formView(dispatch, model),
		tableView(dispatch, model.meals),
		// pre(JSON.stringify(model, null, 2)),
	]);

export default view;
