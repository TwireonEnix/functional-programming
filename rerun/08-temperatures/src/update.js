import * as R from 'ramda';

export const MSGS = {
	LEFT_INPUT: 'LEFT_INPUT',
	RIGHT_INPUT: 'RIGHT_INPUT',
	LEFT_UNIT: 'LEFT_UNIT',
	RIGHT_UNIT: 'RIGHT_UNIT',
};

const toFloat = R.compose(
	R.defaultTo(0),
	parseFloat,
);

export const leftInputMsg = leftValue => ({
	type: MSGS.LEFT_INPUT,
	leftValue,
});

export const rightInputMsg = rightValue => ({
	type: MSGS.RIGHT_INPUT,
	rightValue,
});

export const leftUnitMsg = leftUnit => ({
	type: MSGS.LEFT_UNIT,
	leftUnit,
});

export const rightUnitMsg = rightUnit => ({
	type: MSGS.RIGHT_UNIT,
	rightUnit,
});

/** Temperature conversion formulas */
const FtoC = t => (5 / 9) * (t - 32);
const CtoF = t => (9 / 5) * t + 32;
const KtoC = t => t - 273.15;
const CtoK = t => t + 273.15;
const FtoK = R.pipe(
	FtoC,
	CtoK,
);
const KtoF = R.pipe(
	KtoC,
	CtoF,
);

const UnitConversions = {
	Celsius: {
		Fahrenheit: CtoF,
		Kelvin: CtoK,
	},
	Fahrenheit: {
		Celsius: FtoC,
		Kelvin: FtoK,
	},
	Kelvin: {
		Celsius: KtoC,
		Fahrenheit: KtoF,
	},
};

// Look which fn to use based on the requirements
const convertFromToTemp = (fromUnit, toUnit, temp) => R.pathOr(R.identity, [fromUnit, toUnit], UnitConversions)(temp);

const convert = model => {
	const { leftValue, leftUnit, rightValue, rightUnit } = model;

	const [fromUnit, fromTemp, toUnit] = model.sourceLeft
		? [leftUnit, leftValue, rightUnit]
		: [rightUnit, rightValue, leftUnit];

	const otherValue = R.pipe(convertFromToTemp)(fromUnit, toUnit, fromTemp);

	return model.sourceLeft ? { ...model, rightValue: otherValue } : { ...model, leftValue: otherValue };
};

const update = (msg, model) => {
	switch (msg.type) {
		case MSGS.LEFT_INPUT: {
			if (msg.leftValue === '') return { ...model, sourceLeft: true, leftValue: '', rightValue: '' };
			const leftValue = toFloat(msg.leftValue);
			return convert({ ...model, sourceLeft: true, leftValue });
		}
		case MSGS.RIGHT_INPUT: {
			if (msg.rightValue === '') return { ...model, sourceLeft: true, leftValue: '', rightValue: '' };
			const rightValue = toFloat(msg.rightValue);
			return convert({ ...model, sourceLeft: false, rightValue });
		}
		case MSGS.LEFT_UNIT: {
			const { leftUnit } = msg;
			return convert({ ...model, leftUnit });
		}
		case MSGS.RIGHT_UNIT: {
			const { rightUnit } = msg;
			return convert({ ...model, rightUnit });
		}
	}
};

export default update;
