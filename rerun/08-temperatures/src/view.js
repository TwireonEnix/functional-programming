import * as R from 'ramda';
import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';

const { div, h1, pre, select, option, input } = hh(h);

import { leftInputMsg, rightInputMsg, leftUnitMsg, rightUnitMsg } from './update';

const UNITS = ['Fahrenheit', 'Celsius', 'Kelvin'];

const unitOptions = selectedUnit =>
	R.map(unit => option({ value: unit, selected: selectedUnit === unit }, unit), UNITS);

const unitSection = (dispatch, unit, value, inputMsg, selectMsg) =>
	div({ className: 'w-50 ma1' }, [
		input({
			type: 'text',
			className: 'db w-100 mv2 pa2 input-reset ba',
			value,
			oninput: e => dispatch(inputMsg(e.target.value)),
		}),
		select(
			{
				className: 'db w-100 pa2 ba input-reset br1 bg-white ba b--black',
				onchange: e => dispatch(selectMsg(e.target.value)),
			},
			unitOptions(unit),
		),
	]);

const view = (dispatch, model) =>
	div({ className: 'mw6 center' }, [
		h1({ className: 'f2 pv2 bb' }, 'Temperature Unit Converter'),
		div({ className: 'flex' }, [
			unitSection(dispatch, model.leftUnit, model.leftValue, leftInputMsg, leftUnitMsg),
			unitSection(dispatch, model.rightUnit, model.rightValue, rightInputMsg, rightUnitMsg),
		]),
		pre(JSON.stringify(model, null, 2)),
	]);

export default view;
